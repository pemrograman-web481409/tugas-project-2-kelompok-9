<?php

class Admin_model {
    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;dbname=librarydb', 'username', 'password');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function register_admin($nama, $email, $password) {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $stmt = $this->db->prepare("INSERT INTO admin (nama_admin, email_admin, password_admin) VALUES (?, ?, ?)");
        $stmt->execute([$nama, $email, $hashed_password]);
        return $stmt->rowCount();
    }

    public function login_admin($email, $password) {
        $stmt = $this->db->prepare("SELECT * FROM admin WHERE email_admin = ?");
        $stmt->execute([$email]);
        $admin = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($admin && password_verify($password, $admin['password_admin'])) {
            return $admin;
        } else {
            return false;
        }
    }

    public function add_member($nama, $email, $tanggal) {
        // Lakukan koneksi ke database
        $conn = new mysqli("localhost", "username", "password", "librarydb");
    
        // Periksa koneksi
        if ($conn->connect_error) {
            die("Koneksi ke database gagal: " . $conn->connect_error);
        }
    
        // Siapkan query SQL untuk menambahkan anggota
        $query = "INSERT INTO anggota (nama_anggota, email_anggota, hari_tanggal) VALUES (?, ?, ?)";
    
        // Persiapkan statement SQL
        $statement = $conn->prepare($query);
    
        // Periksa apakah statement berhasil disiapkan
        if (!$statement) {
            die("Error dalam menyiapkan statement: " . $conn->error);
        }
    
        // Bind parameter ke statement
        $statement->bind_param("sss", $nama, $email, $tanggal);
    
        // Jalankan statement
        $result = $statement->execute();
    
        // Tutup statement
        $statement->close();
    
        // Tutup koneksi database
        $conn->close();
    
        // Periksa apakah query berhasil dijalankan
        if ($result) {
            // Query berhasil dijalankan, kembalikan true
            return true;
        } else {
            // Query gagal dijalankan, kembalikan false
            return false;
        }
    }

    public function get_members() {
        // Lakukan koneksi ke database
        $conn = new mysqli("localhost", "username", "password", "librarydb");
    
        // Periksa koneksi
        if ($conn->connect_error) {
            die("Koneksi ke database gagal: " . $conn->connect_error);
        }
    
        // Siapkan query SQL untuk mengambil data anggota
        $query = "SELECT * FROM anggota";
    
        // Eksekusi query
        $result = $conn->query($query);
    
        // Ambil hasil query sebagai array asosiatif
        $members = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $members[] = $row;
            }
        }
    
        // Tutup koneksi database
        $conn->close();
    
        // Kembalikan data anggota
        return $members;
    }
    
    public function get_books() {
        // Lakukan koneksi ke database
        $conn = new mysqli("localhost", "username", "password", "librarydb");
    
        // Periksa koneksi
        if ($conn->connect_error) {
            die("Koneksi ke database gagal: " . $conn->connect_error);
        }
    
        // Siapkan query SQL untuk mengambil data buku
        $query = "SELECT * FROM buku";
    
        // Eksekusi query
        $result = $conn->query($query);
    
        // Ambil hasil query sebagai array asosiatif
        $books = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $books[] = $row;
            }
        }
    
        // Tutup koneksi database
        $conn->close();
    
        // Kembalikan data buku
        return $books;
    }
    
    public function get_loans() {
        // Lakukan koneksi ke database
        $conn = new mysqli("localhost", "username", "password", "librarydb");
    
        // Periksa koneksi
        if ($conn->connect_error) {
            die("Koneksi ke database gagal: " . $conn->connect_error);
        }
    
        // Siapkan query SQL untuk mengambil data peminjaman
        $query = "SELECT * FROM peminjaman";
    
        // Eksekusi query
        $result = $conn->query($query);
    
        // Ambil hasil query sebagai array asosiatif
        $loans = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $loans[] = $row;
            }
        }
    
        // Tutup koneksi database
        $conn->close();
    
        // Kembalikan data peminjaman
        return $loans;
    }

}
?>
