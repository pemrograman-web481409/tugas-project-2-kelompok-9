<!DOCTYPE html>
<html>
<head>
    <title>Daftar Anggota</title>
    <!-- Tautan ke CSS Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="home-style.css">
</head>
<body>
    <div class="container">
        <h2>Daftar Anggota</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Tanggal Bergabung</th>
                </tr>
            </thead>
            <tbody>
                <!-- Loop untuk menampilkan data anggota -->
                <?php foreach ($members as $member): ?>
                <tr>
                    <td><?php echo $member['nama_anggota']; ?></td>
                    <td><?php echo $member['email_anggota']; ?></td>
                    <td><?php echo $member['hari_tanggal']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <a href="index.php?action=dashboard" class="btn btn-primary">Kembali ke Dashboard</a>
    </div>
</body>
</html>
