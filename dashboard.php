<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="home-style.css">
</head>
<body>
    <div class="container">
        <h1>Dashboard Admin</h1>
        <p>Selamat datang, <?php echo htmlspecialchars($_SESSION['admin_name']); ?>!</p>
        <p>Di sini Anda bisa mengatur anggota dan buku.</p>
        <a href="index.php?action=profile" class="btn btn-success">Lihat Profil</a><br><br>
        <a href="add_member.php" class="btn btn-success">Tambah Anggota</a><br><br>
        <a href="index.php?action=view_members" class="btn btn-primary">Lihat Anggota</a><br><br>
        <a href="index.php?action=view_books" class="btn btn-primary">Lihat Buku</a><br><br>
        <a href="index.php?action=view_loans" class="btn btn-primary">Lihat Peminjaman</a><br><br>
        <a href="index.php?action=logout" class="btn btn-danger">Logout</a>
        <!-- Tambahkan konten dashboard lainnya di sini -->
    </div>
</body>
</html>
