<?php
require_once 'admin_controller.php';

session_start();

$controller = new Admin_controller();

$action = isset($_GET['action']) ? $_GET['action'] : 'login';

switch ($action) {
    case 'register':
        $controller->register();
        break;
    case 'login':
        $controller->login();
        break;
    case 'logout':
        $controller->logout();
        break;
    case 'dashboard':
        $controller->dashboard();
        break;
    case 'profile':
        $controller->profile();
        break;
    case 'view_members':
        $controller->view_members();
        break;
    case 'view_books':
        $controller->view_books();
        break;
    case 'view_loans':
        $controller->view_loans();
        break;
    default:
        header("Location: index.php?action=login");
        break;
}
?>
