<?php

require_once 'admin_model.php';

class Admin_controller {
    private $model;

    public function __construct() {
        $this->model = new Admin_model();
    }

    public function register() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $nama = $_POST['nama'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $result = $this->model->register_admin($nama, $email, $password);
            if ($result) {
                echo "Registrasi berhasil!";
            } else {
                echo "Registrasi gagal!";
            }
        } else {
            include 'register_form.php';
        }
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $admin = $this->model->login_admin($email, $password);
            if ($admin) {
                $_SESSION['admin_id'] = $admin['id_admin'];
                $_SESSION['admin_name'] = $admin['nama_admin'];
                $_SESSION['admin_email'] = $admin['email_admin'];
                header("Location: index.php?action=dashboard");
                exit();
            } else {
                echo "Login gagal! Periksa kembali email dan password Anda.";
            }
        } else {
            include 'login_form.php';
        }
    }

    public function logout() {
        session_unset(); // Hapus semua variabel sesi
        session_destroy(); // Hancurkan sesi
        header("Location: index.php?action=login");
        exit();
    }

    public function dashboard() {
        if (!isset($_SESSION['admin_id'])) {
            header("Location: index.php?action=login");
            exit();
        }
        include 'dashboard.php';
    }

    public function profile() {
        if (!isset($_SESSION['admin_id'])) {
            header("Location: index.php?action=login");
            exit();
        }
        include 'profile.php';
    }

    public function add_member() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $nama = $_POST['nama'];
            $email = $_POST['email'];
            $tanggal = $_POST['tanggal'];
            $result = $this->model->add_member($nama, $email, $tanggal);
            if ($result) {
                echo "Anggota berhasil ditambahkan!";
                header("Location: index.php?action=dashboard");
                exit();
            } else {
                echo "Gagal menambahkan anggota!";
            }
        } else {
            include 'add_member.php';
        }
    }
    
    public function view_members() {
        // Panggil metode di model untuk mengambil data anggota
        $members = $this->model->get_members();
        
        // Masukkan data anggota ke dalam tampilan
        include 'view_members.php';
    }
    
    public function view_books() {
        // Panggil metode di model untuk mengambil data buku
        $books = $this->model->get_books();
        
        // Masukkan data buku ke dalam tampilan
        include 'view_books.php';
    }

    public function view_loans() {
        // Panggil metode di model untuk mengambil data peminjaman
        $loans = $this->model->get_loans();
        
        // Masukkan data peminjaman ke dalam tampilan
        include 'view_loans.php';
    }
    
}
?>
