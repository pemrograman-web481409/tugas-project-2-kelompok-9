<!DOCTYPE html>
<html>
<head>
    <title>Daftar Peminjaman Buku</title>
    <!-- Tautan ke CSS Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="home-style.css">
</head>
<body>
    <div class="container">
        <h2>Daftar Peminjaman Buku</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Nama Peminjam</th>
                    <th>Judul Buku</th>
                    <th>Nomor Telepon</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <!-- Loop untuk menampilkan data peminjaman -->
                <?php foreach ($loans as $loan): ?>
                <tr>
                    <td><?php echo $loan['nama_peminjam']; ?></td>
                    <td><?php echo $loan['judul_buku']; ?></td>
                    <td><?php echo $loan['no_tlp']; ?></td>
                    <td><?php echo $loan['tgl_pinjam']; ?></td>
                    <td><?php echo $loan['tgl_kembali']; ?></td>
                    <td><?php echo $loan['status']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <a href="index.php?action=dashboard" class="btn btn-primary">Kembali ke Dashboard</a>
    </div>
</body>
</html>
