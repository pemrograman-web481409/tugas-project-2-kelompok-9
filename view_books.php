<!DOCTYPE html>
<html>
<head>
    <title>Daftar Buku Tersedia</title>
    <!-- Tautan ke CSS Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="home-style.css">
</head>
<body>
    <div class="container">
        <h2>Daftar Buku Tersedia</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Judul</th>
                    <th>Penulis</th>
                    <th>Penerbit</th>
                    <th>Jumlah Halaman</th>
                    <th>Cover</th>
                </tr>
            </thead>
            <tbody>
                <!-- Loop untuk menampilkan data buku -->
                <?php foreach ($books as $book): ?>
                <tr>
                    <td><?php echo $book['judul']; ?></td>
                    <td><?php echo $book['penulis']; ?></td>
                    <td><?php echo $book['penerbit']; ?></td>
                    <td><?php echo $book['jml_hal']; ?></td>
                    <!-- Menampilkan cover (gambar) menggunakan tag <img> -->
                    <td><img src="data:image/jpeg;base64,<?php echo base64_encode($book['cover']); ?>" alt="Cover" style="max-width: 100px;"></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <a href="index.php?action=dashboard" class="btn btn-primary">Kembali ke Dashboard</a>
    </div>
</body>
</html>
