<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="home-style.css">
</head>
<body>
    <div class="container">
        <h1>Profil Admin</h1>
        <table class="table table-bordered">
            <tr>
                <th>Nama</th>
                <td><?php echo htmlspecialchars($_SESSION['admin_name']); ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo htmlspecialchars($_SESSION['admin_email']); ?></td>
            </tr>
        </table>
        <a href="index.php?action=dashboard" class="btn btn-primary">Kembali ke Dashboard</a>
    </div>
</body>
</html>
